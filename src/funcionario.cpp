#include "funcionario.hpp"
#include <iostream>

using namespace std;

Funcionario::Funcionario(){
	
	set_nome("");
	set_dia_nasc(0);
	set_mes_nasc(0);
	set_ano_nasc(0);
	set_cpf("0");
    set_email("@email.com");
    set_telefone(00000000);
	set_salario(0);
	set_funcao("");
	set_dia_contratacao(0);
	set_mes_contratacao(0);
	set_ano_contratacao(0);
}

Funcionario::~Funcionario(){}

void Funcionario::set_salario(float salario){
	this->salario = salario;
}
float Funcionario::get_salario(){
	return salario;
}
void Funcionario::set_funcao(string funcao){
	this->funcao = funcao;
}
string Funcionario::get_funcao(){
	return funcao;
}
void Funcionario::set_dia_contratacao(int dia_contratacao){
	this->dia_contratacao = dia_contratacao;
}
int Funcionario::get_dia_contratacao(){
	return dia_contratacao;
}
void Funcionario::set_mes_contratacao(int mes_contratacao){
	this->mes_contratacao = mes_contratacao;
}
int Funcionario::get_mes_contratacao(){
	return mes_contratacao;
}
void Funcionario::set_ano_contratacao(int ano_contratacao){
	this->ano_contratacao = ano_contratacao;
}
int Funcionario::get_ano_contratacao(){
	return ano_contratacao;
}

void Funcionario::imprime_dados(){
	cout << std::endl << "Nome: " << get_nome() << std::endl;
    cout << "Data de Nascimento: " << get_dia_nasc() << "/" << get_mes_nasc() << "/" << get_ano_nasc() << std::endl;
    cout << "CPF: " << get_cpf() << std::endl;
    cout << "Email: " << get_email() << std::endl;
    cout << "Telefone: " << get_telefone() << std::endl;
	cout << "Salário: " << salario << std::endl;
	cout << "Função: " << funcao << std::endl;
	cout << "Data da contratação: " << dia_contratacao << "/" << mes_contratacao << "/" << ano_contratacao << std::endl;
	
}
