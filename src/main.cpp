#include <iostream>
#include <string>
#include <fstream>
#include <exception>
#include "produto.hpp"
#include "pessoa.hpp"
#include "socio.hpp"
#include "cliente.hpp"
#include "funcionario.hpp"

using namespace std;

void Carregar_Produtos_Armazenados();
void Carregar_Clientes_Armazenados();
void Carregar_Socios_Armazenados();
void Carregar_Funcionarios_Armazenados();
void Cadastrar_Produto();
void Cadastrar_Cliente();
void Cadastrar_Socio();
void Cadastrar_Funcionario();
void Destrutores();

vector <Produto*> Estoque;
vector <Cliente*> Lista_Clientes;
vector <Socio*> Lista_Socios;
vector <Funcionario*> Lista_Funcionarios;
string numero;

int main(int argc, char ** argv){
	
	Carregar_Produtos_Armazenados();
	Carregar_Clientes_Armazenados();
	Carregar_Socios_Armazenados();
	Carregar_Funcionarios_Armazenados();
	
	int executar, modo_estoque, modo_venda, modo_recomendacao, modo_outros;
	cout << endl << "Olá, seja bem-vindo à Padaria" << endl;
	while(executar != 4){
		cout << endl << "Escolha uma Modo ou uma Ação:" << endl;
		cout << "(1) - Modo Venda" << endl;
		cout << "(2) - Modo Estoque" << endl;
		cout << "(3) - Modo Recomendação" << endl;
		cout << "(4) - Encerrar o programa" << endl;
		cout << "(5) - Outro" << endl;
		cin >> executar;
		
		if(executar == 1){
			cout << "Este é o Modo Venda. Quem irá realizar a compra?:" << endl;
			cout << "(1) - Cliente" << endl;
			cout << "(2) - Sócio" << endl;
			cin >> modo_venda;
			if(modo_venda == 1) Cadastrar_Cliente();
			if(modo_venda == 2) Cadastrar_Socio();	
		}
		if(executar == 2){
			cout << "No Modo Estoque você pode:" << endl;
			cout << "(1) - Cadastrar um novo produto" << endl;
			cout << "(2) - Vizualizar os produtos que estão no estoque" << endl;
			cin >> modo_estoque;
			if(modo_estoque == 1) Cadastrar_Produto();
			if(modo_estoque == 2){
				for(auto x: Estoque)
					x->imprime_dados();
				}
		}
		if(executar == 3){
		
		}
		if(executar == 4){
			Destrutores();
		}
		if(executar == 5){
			cout << endl << "Outras opções são: " << endl;
			cout << "(1) - Cadastrar um funcionário" << endl;
			cout << "(2) - Vizualizar a lista de Funcionários" << endl;
			cout << "(3) - Vizualizar a lista de Clientes" << endl;
			cout << "(4) - Vizualizar a lista de Sócios" << endl;
			cin >> modo_outros;
			if(modo_outros == 1) Cadastrar_Funcionario();
			if(modo_outros == 2){
				for(auto x: Lista_Funcionarios)
					x->imprime_dados();
			}
			if(modo_outros == 3){
				for(auto x: Lista_Clientes)
					x->imprime_dados();
			}
			if(modo_outros == 4){
				for(auto x: Lista_Socios)
					x->imprime_dados();
			}
		}
	}	
	return 0;
}

void Carregar_Produtos_Armazenados(){

	string linha("0"), nome_produto, categoria_produto, quantidade_produto_str, preco_produto_str;
	char c = 0;
	int quantidade_produto, j = 0;
	float preco_produto;
	
	fstream file_produto;
	file_produto.open ("doc/produtos.txt", ios::out | ios::in | ios::app);
	
	while(getline(file_produto, linha)) {
	Produto * produto_aux = new Produto();
		for(j=0; c = linha.at(j), c != ';'; j++)
			nome_produto += c;
		j++;
		produto_aux->set_nome(nome_produto);
		
		for(; c = linha.at(j), c != ';'; j++)
			quantidade_produto_str += c;
		j++;
		quantidade_produto = stoi(quantidade_produto_str);
		produto_aux->set_quantidade(quantidade_produto);
		
		for(; c = linha.at(j), c != ';'; j++) {
			if(c == ',') {
				produto_aux->set_categorias(categoria_produto);
				categoria_produto.erase();
			}
			else categoria_produto += c;
		}
		produto_aux->set_categorias(categoria_produto);
		j++;
		
		for(; c = linha.at(j), c != ';'; j++)
			preco_produto_str +=c;
		
		preco_produto = stof(preco_produto_str);
		produto_aux->set_preco(preco_produto);	
		
		nome_produto.erase();
		quantidade_produto_str.erase();
		categoria_produto.erase();
		preco_produto_str.erase();
		Estoque.push_back(produto_aux);
	}
}

void Carregar_Clientes_Armazenados(){

	string linha("0"), nome_cliente, cpf_cliente, email_cliente, dia_nasc_cliente_str, mes_nasc_cliente_str, ano_nasc_cliente_str, telefone_cliente_str;
	char c = 0;
	int dia_nasc_cliente, mes_nasc_cliente, ano_nasc_cliente, telefone_cliente, j = 0;

	fstream file_cliente;
	file_cliente.open ("doc/clientes.txt", ios::out | ios::in | ios::app);
	
	while(getline(file_cliente, linha)) {
	Cliente * cliente_aux = new Cliente();
		for(j=0; c = linha.at(j), c != ';'; j++){
			nome_cliente += c;
		}
		j++;
		cliente_aux->set_nome(nome_cliente);
		
		for(; c = linha.at(j), c != ';'; j++)
			cpf_cliente += c;
		j++;
		cliente_aux->set_cpf(cpf_cliente);
		
		for(; c = linha.at(j), c != ';'; j++)
			dia_nasc_cliente_str +=c;
		j++;
		dia_nasc_cliente = stoi(dia_nasc_cliente_str);
		cliente_aux->set_dia_nasc(dia_nasc_cliente);
		
		for(; c = linha.at(j), c != ';'; j++)
			mes_nasc_cliente_str +=c;
		j++;
		mes_nasc_cliente = stoi(mes_nasc_cliente_str);
		cliente_aux->set_mes_nasc(mes_nasc_cliente);
		
		for(; c = linha.at(j), c != ';'; j++)
			ano_nasc_cliente_str +=c;
		j++;
		ano_nasc_cliente = stoi(ano_nasc_cliente_str);
		cliente_aux->set_ano_nasc(ano_nasc_cliente);
		
		for(; c = linha.at(j), c != ';'; j++)
			telefone_cliente_str +=c;
		j++;
		telefone_cliente = stoi(telefone_cliente_str);
		cliente_aux->set_telefone(telefone_cliente);	
		
		for(; c = linha.at(j), c != ';'; j++)
			email_cliente += c;
		j++;
		cliente_aux->set_email(email_cliente);
		
		nome_cliente.erase();
		cpf_cliente.erase();
		dia_nasc_cliente_str.erase();
		mes_nasc_cliente_str.erase();
		ano_nasc_cliente_str.erase();
		telefone_cliente_str.erase();
		email_cliente.erase();
		Lista_Clientes.push_back(cliente_aux);
	}
}

void Carregar_Socios_Armazenados(){
	
	string linha("0"), nome_socio, cpf_socio, email_socio, dia_nasc_socio_str, mes_nasc_socio_str, ano_nasc_socio_str, telefone_socio_str;
	char c = 0;
	int dia_nasc_socio, mes_nasc_socio, ano_nasc_socio, telefone_socio, j = 0;

	fstream file_socio;
	file_socio.open ("doc/socios.txt", ios::out | ios::in | ios::app);
	
	while(getline(file_socio, linha)) {
	Socio * socio_aux = new Socio();
		for(j=0; c = linha.at(j), c != ';'; j++){
			nome_socio += c;
		}
		j++;
		socio_aux->set_nome(nome_socio);
		
		for(; c = linha.at(j), c != ';'; j++)
			cpf_socio += c;
		j++;
		socio_aux->set_cpf(cpf_socio);
		
		for(; c = linha.at(j), c != ';'; j++)
			dia_nasc_socio_str +=c;
		j++;
		dia_nasc_socio = stoi(dia_nasc_socio_str);
		socio_aux->set_dia_nasc(dia_nasc_socio);
		
		for(; c = linha.at(j), c != ';'; j++)
			mes_nasc_socio_str +=c;
		j++;
		mes_nasc_socio = stoi(mes_nasc_socio_str);
		socio_aux->set_mes_nasc(mes_nasc_socio);
		
		for(; c = linha.at(j), c != ';'; j++)
			ano_nasc_socio_str +=c;
		j++;
		ano_nasc_socio = stoi(ano_nasc_socio_str);
		socio_aux->set_ano_nasc(ano_nasc_socio);
		
		for(; c = linha.at(j), c != ';'; j++)
			telefone_socio_str +=c;
		j++;
		telefone_socio = stoi(telefone_socio_str);
		socio_aux->set_telefone(telefone_socio);	
		
		for(; c = linha.at(j), c != ';'; j++)
			email_socio += c;
		j++;
		socio_aux->set_email(email_socio);
		
		nome_socio.erase();
		cpf_socio.erase();
		dia_nasc_socio_str.erase();
		mes_nasc_socio_str.erase();
		ano_nasc_socio_str.erase();
		telefone_socio_str.erase();
		email_socio.erase();
		Lista_Socios.push_back(socio_aux);
	}
}

void Carregar_Funcionarios_Armazenados(){
	
	string linha("0"), nome_funcionario, cpf_funcionario, email_funcionario, dia_nasc_funcionario_str, mes_nasc_funcionario_str, ano_nasc_funcionario_str, telefone_funcionario_str, salario_funcionario_str, funcao_funcionario, dia_contratacao_funcionario_str, mes_contratacao_funcionario_str, ano_contratacao_funcionario_str;
	char c = 0;
	int dia_nasc_funcionario, mes_nasc_funcionario, ano_nasc_funcionario, telefone_funcionario, dia_contratacao_funcionario, mes_contratacao_funcionario, ano_contratacao_funcionario, j = 0;
	float salario_funcionario;

	fstream file_funcionario;
	file_funcionario.open ("doc/funcionarios.txt", ios::out | ios::in | ios::app);
	
	while(getline(file_funcionario, linha)) {
	Funcionario * funcionario_aux = new Funcionario();
		for(j=0; c = linha.at(j), c != ';'; j++){
			nome_funcionario += c;
		}
		j++;
		funcionario_aux->set_nome(nome_funcionario);
		
		for(; c = linha.at(j), c != ';'; j++)
			cpf_funcionario += c;
		j++;
		funcionario_aux->set_cpf(cpf_funcionario);
		
		for(; c = linha.at(j), c != ';'; j++)
			dia_nasc_funcionario_str +=c;
		j++;
		dia_nasc_funcionario = stoi(dia_nasc_funcionario_str);
		funcionario_aux->set_dia_nasc(dia_nasc_funcionario);
		
		for(; c = linha.at(j), c != ';'; j++)
			mes_nasc_funcionario_str +=c;
		j++;
		mes_nasc_funcionario = stoi(mes_nasc_funcionario_str);
		funcionario_aux->set_mes_nasc(mes_nasc_funcionario);
		
		for(; c = linha.at(j), c != ';'; j++)
			ano_nasc_funcionario_str +=c;
		j++;
		ano_nasc_funcionario = stoi(ano_nasc_funcionario_str);
		funcionario_aux->set_ano_nasc(ano_nasc_funcionario);
		
		for(; c = linha.at(j), c != ';'; j++)
			telefone_funcionario_str +=c;
		j++;
		telefone_funcionario = stoi(telefone_funcionario_str);
		funcionario_aux->set_telefone(telefone_funcionario);	
		
		for(; c = linha.at(j), c != ';'; j++)
			email_funcionario += c;
		j++;
		funcionario_aux->set_email(email_funcionario);
		
		for(; c = linha.at(j), c != ';'; j++)
			salario_funcionario_str +=c;
		j++;
		salario_funcionario = stof(salario_funcionario_str);
		funcionario_aux->set_salario(salario_funcionario);
		
		for(; c = linha.at(j), c != ';'; j++)
			funcao_funcionario += c;
		j++;
		funcionario_aux->set_funcao(funcao_funcionario);
		
		for(; c = linha.at(j), c != ';'; j++)
			dia_contratacao_funcionario_str +=c;
		j++;
		dia_contratacao_funcionario = stoi(dia_contratacao_funcionario_str);
		funcionario_aux->set_dia_contratacao(dia_contratacao_funcionario);
		
		for(; c = linha.at(j), c != ';'; j++)
			mes_contratacao_funcionario_str +=c;
		j++;
		mes_contratacao_funcionario = stoi(mes_contratacao_funcionario_str);
		funcionario_aux->set_mes_contratacao(mes_contratacao_funcionario);
		
		for(; c = linha.at(j), c != ';'; j++)
			ano_contratacao_funcionario_str +=c;
		j++;
		ano_contratacao_funcionario = stoi(ano_contratacao_funcionario_str);
		funcionario_aux->set_ano_contratacao(ano_contratacao_funcionario);
		
		nome_funcionario.erase();
		cpf_funcionario.erase();
		dia_nasc_funcionario_str.erase();
		mes_nasc_funcionario_str.erase();
		ano_nasc_funcionario_str.erase();
		telefone_funcionario_str.erase();
		email_funcionario.erase();
		salario_funcionario_str.erase();
		funcao_funcionario.erase();
		dia_contratacao_funcionario_str.erase();
		mes_contratacao_funcionario_str.erase();
		ano_contratacao_funcionario_str.erase();
		Lista_Funcionarios.push_back(funcionario_aux);
	}
}

void Cadastrar_Produto(){

	string nome_produto, categoria_produto;
	int num_categorias_produto, quantidade_produto;
	float preco_produto;
	bool novo_produto = false;
	
	fstream file_produto;
	file_produto.open ("doc/produtos.txt", ios::out | ios::in | ios::app);
				
	cout << "Digite o nome do produto (sem acentos gráficos): ";
	cin >> nome_produto;
	cout << "Digite a quantidade desse produto: ";
	cin >> quantidade_produto;
	
	for(auto x: Estoque){
		if(x->get_nome() == nome_produto){
			x->set_quantidade(x->get_quantidade()+quantidade_produto);
			novo_produto = true;
			cout << "Já existe um produto com esse nome. Sua quantidade foi acrescida no estoque em " << quantidade_produto << " unidades" << endl;
			break;
		}
	}
	
	if(novo_produto == false){
		Produto * produto_aux = new Produto();
		produto_aux->set_nome(nome_produto);
		file_produto << produto_aux->get_nome() << ";";
		
		produto_aux->set_quantidade(quantidade_produto);
		file_produto << produto_aux->get_quantidade() << ";";
		cout << "Quantas categorias possui este produto? ";
		cin >> num_categorias_produto;
		for(int c=0; c < num_categorias_produto; c++){
			cout << "Digite uma categoria: ";
			cin >> categoria_produto;
			file_produto << categoria_produto;
			if(c+1 != num_categorias_produto)
				file_produto << ",";
			produto_aux->set_categorias(categoria_produto);
		}
		file_produto << ";";
		cout << "Digite o preço do produto (separe os centavos com ponto): ";
		cin >> preco_produto;
		produto_aux->set_preco(preco_produto);
		file_produto << produto_aux->get_preco() << ";" << endl;
		Estoque.push_back(produto_aux);
		file_produto.close();
	}
}

void Cadastrar_Cliente(){
	
	string nome_cliente, cpf_cliente, email_cliente;
	int dia_nasc_cliente = 0, mes_nasc_cliente = 0, ano_nasc_cliente = 0, telefone_cliente = 0;
	int indice = 1, opcao = 1, produto_indice, produto_quantidade;
	float total = 0;
	bool novo_cliente = false;
	vector <int> num_produto;
	vector <int> qtds_produto;
	
	fstream file_clientes;
	file_clientes.open ("doc/clientes.txt", ios::out | ios::in | ios::app);	
	
	cout << "Qual é o seu primeiro nome? ";
	cin >> nome_cliente;
	cout << "Qual é o seu CPF? ";
	cin >> cpf_cliente;
	
	for(auto x: Lista_Clientes){
		if(x->get_nome() == nome_cliente){
			if(x->get_cpf()	 == cpf_cliente){
			novo_cliente = true;
			cout << endl << "Você já está cadastrado como um cliente." << endl << endl;
			break;
			}
		}
	}
	
	if(novo_cliente == false){
		Cliente * cliente_aux = new Cliente();
		cliente_aux->set_nome(nome_cliente);
		file_clientes << cliente_aux->get_nome() << ";";
		
		cliente_aux->set_cpf(cpf_cliente);
		file_clientes << cliente_aux->get_cpf() << ";";
		
		cout << "Digite o dia em que você nasceu: ";
		while(dia_nasc_cliente < 1 || dia_nasc_cliente > 31){
			cin >> numero;
			try{
				dia_nasc_cliente = stoi(numero);
				if(dia_nasc_cliente < 1 || dia_nasc_cliente > 31)
					throw(1);
			}
			catch(int e){ cout << "O dia de nascimento deve ser um número entre 1 e 31. Por favor, digite-o novamente: ";}
			catch(exception& e){ cout << "Erro de leitura! Digite APENAS o dia de nascimento: ";}
			numero.erase();
		}
		cliente_aux->set_dia_nasc(dia_nasc_cliente);
		file_clientes << cliente_aux->get_dia_nasc() << ";";
		cout << "Digite o mes em que você nasceu: ";
		while(mes_nasc_cliente < 1 || mes_nasc_cliente > 12){
			cin >> numero;
			try{
				mes_nasc_cliente = stoi(numero);
				if(mes_nasc_cliente < 1 || mes_nasc_cliente > 12)
					throw(1);
			}
			catch(int e){ cout << "O mes de nascimento deve ser um número entre 1 e 12. Por favor, digite-o novamente.";}
			catch(exception& e){ cout << "Erro de leitura! Digite APENAS o mes de nascimento: ";}
			numero.erase();
		}
		cliente_aux->set_mes_nasc(mes_nasc_cliente);
		file_clientes << cliente_aux->get_mes_nasc() << ";";
		cout << "Digite o ano em que você nasceu: ";
		cin >> ano_nasc_cliente;
		while(ano_nasc_cliente < 1900 || ano_nasc_cliente > 2019){
			cout << "O ano de nascimento deve ser um número entre 1900 e 2019. Por favor, digite-o novamente.";
			cin >> ano_nasc_cliente;
		}
		cliente_aux->set_ano_nasc(ano_nasc_cliente);
		file_clientes << cliente_aux->get_ano_nasc() << ";";
		cout << "Digite seu telefone (ele deve conter nove dígitos): ";
		cin >> telefone_cliente;
		while(telefone_cliente > 999999999 || telefone_cliente < 100000000){
			cout << "O telefone deve conter 9 dígitos, não começando com zero. Por favor, digite-o novamente.";
			cin >> telefone_cliente;
		}
		cliente_aux->set_telefone(telefone_cliente);
		file_clientes << cliente_aux->get_telefone() << ";" << endl;
		cout << "Digite seu email: ";
		cin >> email_cliente;
		cliente_aux->set_email(email_cliente);
		file_clientes << cliente_aux->get_email() << ";" << endl;
		
		Lista_Clientes.push_back(cliente_aux);
		file_clientes.close();
	}	
	// MODO VENDA
	cout << endl << "ESTOQUE DE PRODUTOS:" << endl << endl;
	while(opcao !=2){
		indice = 1;		
		for(auto x: Estoque){
		cout << "(" << indice << ")";
		x->imprime_dados();
		cout << endl;
		indice++;
		}
		cout << "Digite o índice de um produto para adicionar ao carrinho: ";
		cin >> produto_indice;
		num_produto.push_back(produto_indice);
		cout << "Digite quantas unidades deste produto serão adicionadas ao carrinho: ";
		cin >> produto_quantidade;
		qtds_produto.push_back(produto_quantidade);
		
		cout << "Digite:" << endl << "(1) - Para escolher outro produto" << endl << "(2) - Para finalizar a compra" << endl;
		cin >> opcao;
	}
	cout << endl << endl << "Produtos Selecionados: "<< endl << endl;
	vector <float> precos;
	int qtd_atual = 0;
	for(int j = 0; j < num_produto.size(); j++) {
		precos.push_back(Estoque[num_produto[j]-1]->get_preco());
		cout << "Produto: " << Estoque[num_produto[j]-1]->get_nome() << "\t";
		cout << "Preço: " << Estoque[num_produto[j]-1]->get_preco() << "\t"; 
		cout << "Quantidade: " << qtds_produto[j] << endl;
		
		qtd_atual = Estoque[num_produto[j]-1]->get_quantidade()-qtds_produto[j];
		Estoque[num_produto[j]-1]->set_quantidade(qtd_atual);		
		total += qtds_produto[j] * precos[j];
	}

	cout << endl << "Valor do desconto: RS 0.00" << endl;
	cout << "Valor total da compra: R$ " << total << endl << endl;
}

void Cadastrar_Socio(){
	string nome_socio, cpf_socio, email_socio;
	int dia_nasc_socio, mes_nasc_socio, ano_nasc_socio, telefone_socio;
	int indice = 1, opcao = 1, produto_indice, produto_quantidade;
	float total = 0;
	bool novo_socio = false;
	vector <int> num_produto;
	vector <int> qtds_produto;
	
	fstream file_socios;
	file_socios.open ("doc/socios.txt", ios::out | ios::in | ios::app);	
	
	cout << "Qual é o seu primeiro nome? ";
	cin >> nome_socio;
	cout << "Qual é o seu CPF? ";
	cin >> cpf_socio;
	
	for(auto x: Lista_Socios){
		if(x->get_nome() == nome_socio){
			if(x->get_cpf()	 == cpf_socio){
			novo_socio = true;
			cout << endl << "Você já está cadastrado como um sócio." << endl << endl;
			break;
			}
		}
	}
	
	if(novo_socio == false){
		Socio * socio_aux = new Socio();
		socio_aux->set_nome(nome_socio);
		file_socios << socio_aux->get_nome() << ";";
		
		socio_aux->set_cpf(cpf_socio);
		file_socios << socio_aux->get_cpf() << ";";
		
		cout << "Digite o dia em que você nasceu: ";
		while(dia_nasc_socio < 1 || dia_nasc_socio > 31){
			cin >> numero;
			try{
				dia_nasc_socio = stoi(numero);
				if(dia_nasc_socio < 1 || dia_nasc_socio > 31)
					throw(1);
			}
			catch(int e){ cout << "O dia de nascimento deve ser um número entre 1 e 31. Por favor, digite-o novamente: ";}
			catch(exception& e){ cout << "Erro de leitura! Digite APENAS o dia de nascimento: ";}
			numero.erase();
		}
		socio_aux->set_dia_nasc(dia_nasc_socio);
		file_socios << socio_aux->get_dia_nasc() << ";";
		cout << "Digite o mes em que você nasceu: ";
		cin >> mes_nasc_socio;
		while(mes_nasc_socio < 1 || mes_nasc_socio > 12){
			cout << "O mes de nascimento deve ser um número entre 1 e 12. Por favor, digite-o novamente.";
			cin >> mes_nasc_socio;
		}
		socio_aux->set_mes_nasc(mes_nasc_socio);
		file_socios << socio_aux->get_mes_nasc() << ";";
		cout << "Digite o ano em que você nasceu: ";
		cin >> ano_nasc_socio;
		while(ano_nasc_socio < 1900 || ano_nasc_socio > 2019){
			cout << "O ano de nascimento deve ser um número entre 1900 e 2019. Por favor, digite-o novamente.";
			cin >> ano_nasc_socio;
		}
		socio_aux->set_ano_nasc(ano_nasc_socio);
		file_socios << socio_aux->get_ano_nasc() << ";";
		cout << "Digite seu telefone (ele deve conter nove dígitos) ";
		cin >> telefone_socio;
		while(telefone_socio > 999999999 || telefone_socio < 100000000){
			cout << "O telefone deve conter 9 dígitos, não começando com zero. Por favor, digite-o novamente.";
			cin >> telefone_socio;
		}
		socio_aux->set_telefone(telefone_socio);
		file_socios << socio_aux->get_telefone() << ";";
		cout << "Digite seu email: ";
		cin >> email_socio;
		socio_aux->set_email(email_socio);
		file_socios << socio_aux->get_email() << ";" << endl;
		
		Lista_Socios.push_back(socio_aux);
		file_socios.close();
	}
	
	// MODO VENDA
	cout << endl << "ESTOQUE DE PRODUTOS:" << endl << endl;
	while(opcao !=2){
		indice = 1;		
		for(auto x: Estoque){
		cout << "(" << indice << ")";
		x->imprime_dados();
		cout << endl;
		indice++;
		}
		cout << "Digite o índice de um produto para adicionar ao carrinho: ";
		cin >> produto_indice;
		num_produto.push_back(produto_indice);
		cout << "Digite quantas unidades deste produto serão adicionadas ao carrinho: ";
		cin >> produto_quantidade;
		qtds_produto.push_back(produto_quantidade);
		
		cout << "Digite:" << endl << "(1) - Para escolher outro produto" << endl << "(2) - Para finalizar a compra" << endl;
		cin >> opcao;
	}
	cout << endl << endl << "Produtos Selecionados: "<< endl << endl;
	vector <float> precos;
	int qtd_atual = 0;
	for(int j = 0; j < num_produto.size(); j++) {
		precos.push_back(Estoque[num_produto[j]-1]->get_preco());
		cout << "Produto: " << Estoque[num_produto[j]-1]->get_nome() << "\t";
		cout << "Preço: " << Estoque[num_produto[j]-1]->get_preco() << "\t"; 
		cout << "Quantidade: " << qtds_produto[j] << endl;
		
		qtd_atual = Estoque[num_produto[j]-1]->get_quantidade()-qtds_produto[j];
		Estoque[num_produto[j]-1]->set_quantidade(qtd_atual);		
		total += qtds_produto[j] * precos[j];
	}
	float desconto;
	desconto = total * 0.15;
	cout << endl << "Valor do desconto: RS " << desconto << endl;
	cout << "Valor total da compra: R$ " << total << endl << endl;
}

void Cadastrar_Funcionario(){

	string nome_funcionario, cpf_funcionario, email_funcionario, funcao_funcionario;
	int dia_nasc_funcionario, mes_nasc_funcionario, ano_nasc_funcionario, telefone_funcionario, dia_contratacao_funcionario, mes_contratacao_funcionario, ano_contratacao_funcionario;
	float salario_funcionario;
	bool novo_funcionario = false;
	
	fstream file_funcionarios;
	file_funcionarios.open ("doc/funcionarios.txt", ios::out | ios::in | ios::app);	
	
	cout << "Qual é o seu primeiro nome? ";
	cin >> nome_funcionario;
	cout << "Qual é o seu CPF? ";
	cin >> cpf_funcionario;
	
	for(auto x: Lista_Funcionarios){
		if(x->get_nome() == nome_funcionario){
			if(x->get_cpf()	 == cpf_funcionario){
			novo_funcionario = true;
			cout << "Você já está cadastrado como um funcionario." << endl;
			break;
			}
		}
	}
	
	if(novo_funcionario == false){
		Funcionario * funcionario_aux = new Funcionario();
		funcionario_aux->set_nome(nome_funcionario);
		file_funcionarios << funcionario_aux->get_nome() << ";";
		
		funcionario_aux->set_cpf(cpf_funcionario);
		file_funcionarios << funcionario_aux->get_cpf() << ";";
		
		cout << "Digite o dia em que você nasceu: ";
		while(dia_nasc_funcionario < 1 || dia_nasc_funcionario > 31){
			cin >> numero;
			try{
				dia_nasc_funcionario = stoi(numero);
				if(dia_nasc_funcionario < 1 || dia_nasc_funcionario > 31)
					throw(1);
			}
			catch(int e){ cout << "O dia de nascimento deve ser um número entre 1 e 31. Por favor, digite-o novamente: ";}
			catch(exception& e){ cout << "Erro de leitura! Digite APENAS o dia de nascimento: ";}
			numero.erase();
		}
		funcionario_aux->set_dia_nasc(dia_nasc_funcionario);
		file_funcionarios << funcionario_aux->get_dia_nasc() << ";";
		cout << "Digite o mes em que você nasceu: ";
		cin >> mes_nasc_funcionario;
		while(mes_nasc_funcionario < 1 || mes_nasc_funcionario > 12){
			cout << "O mes de nascimento deve ser um número entre 1 e 12. Por favor, digite-o novamente.";
			cin >> mes_nasc_funcionario;
		}
		funcionario_aux->set_mes_nasc(mes_nasc_funcionario);
		file_funcionarios << funcionario_aux->get_mes_nasc() << ";";
		cout << "Digite o ano em que você nasceu: ";
		cin >> ano_nasc_funcionario;
		while(ano_nasc_funcionario < 1900 || ano_nasc_funcionario > 2019){
			cout << "O ano de nascimento deve ser um número entre 1900 e 2019. Por favor, digite-o novamente.";
			cin >> ano_nasc_funcionario;
		}
		funcionario_aux->set_ano_nasc(ano_nasc_funcionario);
		file_funcionarios << funcionario_aux->get_ano_nasc() << ";";
		cout << "Digite seu telefone (ele deve conter nove dígitos): ";
		cin >> telefone_funcionario;
		while(telefone_funcionario > 999999999 || telefone_funcionario < 100000000){
			cout << "O telefone deve conter 9 dígitos, não começando com zero. Por favor, digite-o novamente.";
			cin >> telefone_funcionario;
		}
		funcionario_aux->set_telefone(telefone_funcionario);
		file_funcionarios << funcionario_aux->get_telefone() << ";";
		cout << "Digite seu email: ";
		cin >> email_funcionario;
		funcionario_aux->set_email(email_funcionario);
		file_funcionarios << funcionario_aux->get_email() << ";";
		cout << "Digite o salário do funcionário: ";
		cin >> salario_funcionario;
		funcionario_aux->set_salario(salario_funcionario);
		file_funcionarios << funcionario_aux->get_salario() << ";";
		cout << "Digite a função do funcionário: ";
		cin >> funcao_funcionario;
		funcionario_aux->set_funcao(funcao_funcionario);
		file_funcionarios << funcionario_aux->get_funcao() << ";";
		cout << "Digite o dia da contratação do funcionário: ";
		while(dia_contratacao_funcionario < 1 || dia_contratacao_funcionario > 31){
			cin >> numero;
			try{
				dia_contratacao_funcionario = stoi(numero);
				if(dia_contratacao_funcionario < 1 || dia_contratacao_funcionario > 31)
					throw(1);
			}
			catch(int e){ cout << "O dia da contratação deve ser um número entre 1 e 31. Por favor, digite-o novamente: ";}
			catch(exception& e){ cout << "Erro de leitura! Digite APENAS o dia de nascimento: ";}
			numero.erase();
		}
		file_funcionarios << funcionario_aux->get_dia_contratacao() << ";";
		cout << "Digite o mes da contratação do funcionário: ";
		cin >> mes_contratacao_funcionario;
		while(mes_contratacao_funcionario < 1 || mes_contratacao_funcionario > 12){
			cout << "O mes da contratacao deve ser um número entre 1 e 12. Por favor, digite-o novamente.";
			cin >> mes_contratacao_funcionario;
		}
		funcionario_aux->set_mes_contratacao(mes_contratacao_funcionario);
		file_funcionarios << funcionario_aux->get_mes_contratacao() << ";";
		cout << "Digite o ano da contratação do funcionário: ";
		cin >> ano_contratacao_funcionario;
		while(ano_contratacao_funcionario < 1900 || ano_contratacao_funcionario > 2019){
			cout << "O ano da contratacao deve ser um número entre 1900 e 2019. Por favor, digite-o novamente.";
			cin >> ano_contratacao_funcionario;
		}	
		funcionario_aux->set_ano_contratacao(ano_contratacao_funcionario);
		file_funcionarios << funcionario_aux->get_ano_contratacao() << ";" << endl;

		Lista_Funcionarios.push_back(funcionario_aux);
		file_funcionarios.close();
	}	
}

void Destrutores(){
	for(auto x: Estoque)
		delete x;
	for(auto x: Lista_Socios)
		delete x;
	for(auto x: Lista_Clientes)
		delete x;
	for(auto x: Lista_Funcionarios)
		delete x;
	Estoque.clear();
	Lista_Socios.clear();
	Lista_Clientes.clear();
	Lista_Funcionarios.clear();
}
