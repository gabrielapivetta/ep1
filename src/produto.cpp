#include "produto.hpp"
#include <iostream>

using namespace std;

Produto::Produto(){
	nome = " ";
	quantidade = 0;
	preco = 0.0f;
}
Produto::~Produto(){}

void Produto::set_nome(string nome){
	this->nome = nome;
}
string Produto::get_nome(){
	return nome;
}
void Produto::set_quantidade(int quantidade){
	this->quantidade = quantidade;
}
int Produto::get_quantidade(){
	return quantidade;
}
void Produto::set_preco(float preco){
	this->preco = preco;
}
float Produto::get_preco(){
	return preco;
}
/*
void Produto::set_categorias(){
	int quantidade_de_categorias;
	string categoria_lida;
	cout << "Quantas categorias possui este produto?\n";
	cin >> quantidade_de_categorias;
	cout << "Digite as categoria do produto a ser adicionado ao estoque\n";
	for(int i=0; i < quantidade_de_categorias; i++){
		cin >> categoria_lida;
		categorias.insert(categoria_lida);
	}
}*/
void Produto::set_categorias(string categoria){
	categorias.insert(categoria);
}
void Produto::get_categorias(){
	for(auto x: categorias)
	cout << x << ". ";
}
void Produto::imprime_dados(){
	cout << "Nome: " << nome << endl;
	cout << "Preço: " << preco << endl;
	cout << "Quantidade: " << quantidade << endl;
	cout << "Categoria(s): ";
	get_categorias();
	cout << endl << endl;
}
