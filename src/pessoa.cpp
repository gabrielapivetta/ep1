#include "pessoa.hpp"
#include <iostream>

using namespace std;

Pessoa::Pessoa(){
	
	nome = "";
	dia_nasc = 0;
	mes_nasc = 0;
	ano_nasc = 0;
	cpf = "0";
	email = "@email.com";
	telefone = 000000000;
}

Pessoa::~Pessoa(){}

void Pessoa::set_nome(string nome){
	this->nome = nome;
}
string Pessoa::get_nome(){
	return nome;
}
void Pessoa::set_dia_nasc(int dia_nasc){
	this->dia_nasc = dia_nasc;
}
int Pessoa::get_dia_nasc(){
	return dia_nasc;
}
void Pessoa::set_mes_nasc(int mes_nasc){
	this->mes_nasc = mes_nasc;
}
int Pessoa::get_mes_nasc(){
	return mes_nasc;
}
void Pessoa::set_ano_nasc(int ano_nasc){
	this->ano_nasc = ano_nasc;
}
int Pessoa::get_ano_nasc(){
	return ano_nasc;
}
void Pessoa::set_cpf(string cpf){
	this->cpf = cpf;
}
string Pessoa::get_cpf(){
	return cpf;
}
void Pessoa::set_email(string email){
	this->email = email;
}
string Pessoa::get_email(){
	return email;
}
void Pessoa::set_telefone(int telefone){
	this->telefone = telefone;
}
int Pessoa::get_telefone(){
 	return telefone;
}

void Pessoa::imprime_dados(){
	std::cout << std::endl << "Nome: " << nome << std::endl;
    std::cout << "Data de Nascimento: " << dia_nasc << "/" << mes_nasc << "/" << ano_nasc << std::endl;
    std::cout << "CPF: " << cpf << std::endl;
    std::cout << "Email: " << email << std::endl;
    std::cout << "Telefone: " << telefone << std::endl;
}
