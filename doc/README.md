Gabriela da Gama Pivetta
Matrícula: 18/0052845

	O trabalho ep1 de orientação a objetos foi feito baseando se no sistema de uma Padaria.
A pasta contém cinco classes com arquivos cpp e hpp (Pessoa, Cliente, Sócio, Funcionário e Produto), onde Cliente e Funcionário herdam de Pessoa e Sócio herda de Cliente. Há também arquivos txt para armazenar os conteúdos de uma cada delas e uma main, que carrega o que está armazenado nos arquivos txt para vectors de cada classe (Lista_Clientes, Lista_Socios, Lista_Funcionarios, Estoque). A main também possui um menu para o usuário escolher entre os modos ofericidos, cadastrar novos Funcionários, Clientes, Sócios e Produtos e vizualizar a lista de cada um desses já cadastrados anteriormente.
