#include <string>
#include "pessoa.hpp"

using namespace std;

class Funcionario: public Pessoa{
	private:
	float salario;
	string funcao;
	int dia_contratacao;
	int mes_contratacao;
	int ano_contratacao;
	
	public:
	Funcionario();
	~Funcionario();
	void set_salario(float salario);
	float get_salario();
	void set_funcao(string funcao);
	string get_funcao();
	void set_dia_contratacao(int dia_contratacao);
	int get_dia_contratacao();
	void set_mes_contratacao(int mes_contratacao);
	int get_mes_contratacao();
	void set_ano_contratacao(int ano_contratacao);
	int get_ano_contratacao();
	void imprime_dados();
};
