#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <string>

using namespace std;

class Pessoa{

	private:
	string nome;
	int dia_nasc;
	int mes_nasc;
	int ano_nasc;
	string cpf;
	string email;
	int telefone;
	
	public:
	Pessoa();
	~Pessoa();
	
	void set_nome(string nome);
	string get_nome();
	void set_dia_nasc(int dia_nasc);
	int get_dia_nasc();
	void set_mes_nasc(int mes_nasc);
	int get_mes_nasc();
	void set_ano_nasc(int ano_nasc);
	int get_ano_nasc();
	void set_cpf(string cpf);
	string get_cpf();
	void set_email(string email);
	string get_email();
	void set_telefone(int telefone);
	int get_telefone();
	
	void imprime_dados();
};

#endif
