#ifndef PRODUTO_HPP
#define PRODUTO_HPP
#include <string>
#include <set>

using namespace std;

class Produto{

	private:
	string nome;
	set<string> categorias;
	float preco;
	int quantidade;
	
	public:
	Produto();
	~Produto();
	
	void set_nome(string nome);
	string get_nome();
	void set_categorias(string categoria);
	void get_categorias();
	void set_preco(float preco);
	float get_preco();
	void set_quantidade(int quantidade);
	int get_quantidade(); 
	void imprime_dados();
	
};

#endif
